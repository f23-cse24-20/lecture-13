#include <iostream>
using namespace std;

int main() {

    /*
        Ask user to input 3 integers.
        Display each input.

        Example:
        You entered: 3
        You entered: 5
        You entered: 7
    */

    // int x, y, z;
    // cin >> x >> y >> z;

    // cout << "You entered: " << x << endl;
    // cout << "You entered: " << y << endl;
    // cout << "You entered: " << z << endl;

    int x;

    for (int i = 0; i < 3; i++) {
        cin >> x;
        cout << "You entered: " << x << endl;
    }

    return 0;
}