#include <iostream>
using namespace std;

int main() {

    /*
        Print "Hello World!" 5 times.

        Print "Hello World!" n times.
    */

    int repeats;
    cin >> repeats;

    for (int i = 0; i < repeats; i++) {
        cout << "Hello World!" << endl;
    }

    return 0;
}