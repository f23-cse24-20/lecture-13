#include <iostream>
using namespace std;

int main() {

    /*
        Ask user for message and display a fixed width banner.
        The width should always be 50 characters.

        Example:

        ==================================================
        | UC Merced                                      |
        ==================================================
    */

    string message;
    getline(cin, message);

    int length = message.length();

    for (int i = 0; i < 50; i++) {
        cout << "=";
    }
    cout << endl;

    cout << "| " << message;
    
    for (int i = 0; i < 50 - length - 4; i++) {
        cout << " ";
    }

    cout << " |" << endl;

    for (int i = 0; i < 50; i++) {
        cout << "=";
    }
    cout << endl;

    return 0;
}