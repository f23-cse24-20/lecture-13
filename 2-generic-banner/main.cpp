#include <iostream>
using namespace std;

int main() {

    /*
        Ask user for message and display a banner.

        Example:

        =========
        UC Merced
        =========
    */

    string message;
    getline(cin, message);

    // int length = message.length();

    for (int i = 0; i < message.length(); i++) {
        cout << "=";
    }
    cout << endl;

    cout << message << endl;

    for (int i = 0; i < message.length(); i++) {
        cout << "=";
    }
    cout << endl;

    return 0;
}