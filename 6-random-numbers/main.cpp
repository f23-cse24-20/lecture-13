#include <iostream>
#include <ucm_random>
using namespace std;

int main() {

    /*
        Generate n random numbers between 100 and 1000.

        Example:
        237
        649
        334
        719
        925
    */

    RNG generator;

    int n;
    cin >> n;

    for (int i = 0; i < n; i++) {
        int x = generator.get(100, 1000);
        cout << x << endl;
    }



    return 0;
}