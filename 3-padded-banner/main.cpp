#include <iostream>
using namespace std;

int main() {

    /*
        Ask user for message and display a padded banner.
        The padding should be 1 character.

        Example:

        ===========
         UC Merced 
        ===========
    */

    string message;
    getline(cin, message);

    int length = message.length();

    for (int i = 0; i < length + 2; i++) {
        cout << "=";
    }
    cout << endl;

    cout << " " << message << " " << endl;

    for (int i = 0; i < length + 2; i++) {
        cout << "=";
    }
    cout << endl;

    return 0;
}